// File MergSortDesc.java

public class MergSortDesc implements Problem, Solution
{
    private int[] arr;
    private int   first;
    private int   last;    
    public int[]  copyBuffer;
    
    public MergSortDesc (int[] arr, int first, int last) 
    {   this.arr   = arr;  
        this.first = first;
        this.last  = last;
 /*      System.out.println ("\nfirst="+first+"last="+last+"MergSortDesc *");
        for (int i=first; i<arr.length; i++)
				System.out.print ("^ " + arr[i] );  
	 	  System.out.println();
*/
    }
    public int[] getArr ()   { return arr;   }
    public int   getFirst () { return first; }
    public int   getLast ()  { return last;  }
    public int[] getCopyBuffer() {return copyBuffer;  }
    public void  setCopyBuffer( int []a ) { copyBuffer = a;  } 


}
