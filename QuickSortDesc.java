// File QuickSortDesc.java

public class QuickSortDesc extends Object implements Problem, Solution
{
    private Object [] arr;
    private int   first;
    private int   last;
    
    public QuickSortDesc (Object[] arr, int first, int last) 
    {   
    	  this.arr   = arr;  
        this.first = first;
        this.last  = last;
    }
    public Object[] getArr ()   { return arr;   }
    public int   getFirst () { return first; }
    public int   getLast ()  { return last;  }

}
