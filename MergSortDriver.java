// File MerSortDriver.java


public class MergSortDriver
{
    public static void main (String[] args)
    {  
		  System.out.println("* MergSortDriver to Test MERG SORT * Gang Wang *\n");
    	  final  int MAX = 20;
    	  MergSort ms = new MergSort();
    	  DivConqContext dcc = new DivConqContext( ms);

        int[] tosort = new int[MAX];
		  for ( int i=0; i<tosort.length; i++ )
				tosort[i] = (int)(Math.random()*100);
			

        System.out.println("Unsorted Array:\t" + printIntArr(tosort));
        Problem p = new MergSortDesc(tosort, 0, tosort.length-1);
        dcc.solve( p );
        
        System.out.println("\nSorted by Gang:\t" 
        		+ printIntArr (((MergSortDesc)p).getCopyBuffer())   );
    }

    private static String printIntArr (int[] arr)
    {   String outstr = "[ ";
        int i = 0;
        for( ; i < arr.length-1; i++)
        {  outstr = outstr + arr[i] + "," ;   }
        outstr = outstr + arr[i] + " ]";
        return outstr;
    }
}


