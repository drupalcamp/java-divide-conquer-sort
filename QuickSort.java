// File QuickSort.java

public class QuickSort extends DivConqStrategy
{    
    public boolean isSimple (Problem p)
    {   return ( ((QuickSortDesc)p).getFirst() >=
                 ((QuickSortDesc)p).getLast()     );
    }

    public Solution simplySolve (Problem p)
    {   return (Solution) p ;   }
    
    public Problem[] decompose (Problem p)
    {   int first = ((QuickSortDesc)p).getFirst();
        int last  = ((QuickSortDesc)p).getLast();
        Object [] a   = ((QuickSortDesc)p).getArr ();
/*        
        System.out.print("a.length= " + a.length + "   a[]=");
        for (int i=0; i<= last; i++)
        		System.out.print( a[i]+ " ," );
        System.out.println("");
*/        
        Object x     = a[first]; // pivot value
        int sp    = first;
        for (int i = first + 1; i <= last; i++)
        {   if (((Student)a[i]).lessThan( (Student) x) )
            {   swap (a, ++sp, i);   }
        }
        swap (a, first, sp);
        
        
        Problem[] ps = new QuickSortDesc[2];
        ps[0] = new QuickSortDesc(a, first, sp-1);
        ps[1] = new QuickSortDesc(a, sp+1, last);
        return ps;
    }
    
    public  Solution combine (Problem p, Solution[] ss)
    {   return (Solution) p;   }
    
    private void swap (Object [] a, int first, int last)
    {   Object temp = a[first];
        a[first] = a[last];
        a[last]  = temp;
    }
	

}



