// File DivConqStrategy.java

abstract public class DivConqStrategy
{   
    abstract public boolean isSimple (Problem p);
    abstract public Solution simplySolve (Problem p);
    abstract public Problem[] decompose (Problem p);
    abstract public Solution combine (Problem p, Solution[] ss);
}
