// File   Student.java

public class Student extends Object
{
	private String name;
	private String id;
	
	public Student ()	{	}
	
	public Student ( String name, String id )
	{
		this.name =  name;
		this.id   =  id;
	}
	
	public boolean equalTo( Student std){
		if ( (this.name).equals(std.name) && (this.id).equals(std.id) )
			return true;
		else return false;
	}
	
	public boolean lessThan( Student std){
		if ( (this.name).compareTo(std.name) < 0 )
			return true;
		else if ((this.name).equals(std.name) && (this.id).compareTo(std.id) < 0 ) 
			return true;
		else return false;
}
	
	public boolean greaterThan( Student std){
		if ( (this.name).compareTo(std.name) > 0 )
			return true;
		else if ((this.name).equals(std.name) && (this.id).compareTo(std.id) > 0 )
			return true;
		else return false;
	}

	public String toStdString(){
		return ( name + " " + id );
	}
	
	public String getName() {  return name; }
	public String getId()   {  return id;  }

}