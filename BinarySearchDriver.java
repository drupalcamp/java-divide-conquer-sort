// File BinarySearchDriver.java

public class BinarySearchDriver
{
    public static void main (String[] args)
    {  
    	System.out.println("* BinarySearchDriver to Test BINARY SEARCH * Gang Wang *\n");
    	
    	BinarySearch qs = new BinarySearch();
    	DivConqContext dcc = new DivConqContext( qs);
    	  
    	Solution s;
    	BinarySearchDesc bsd;
     	int[] a = {1, 2, 3, 5, 6, 7, 8, 9, 10,13,40,46,99,100,200};
   	System.out.println("Array:  " + printIntArr(a) + "\n");
    	s = dcc.solve( new BinarySearchDesc(200, a, 0, a.length-1, false) );
    	bsd = (BinarySearchDesc) s;
     	System.out.println ("* Target = " + bsd.getTarget() + 
        		"\t\t* Find Target In Array: " +  bsd.getFound() + "\n");

    	s = dcc.solve( new BinarySearchDesc(150, a, 0, a.length-1, false) );
    	bsd = (BinarySearchDesc) s;
     	System.out.println ("* Target = " + bsd.getTarget() + 
        		"\t\t* Find Target In Array: " +  bsd.getFound() + "\n");

    	s = dcc.solve( new BinarySearchDesc(4, a, 0, a.length-1, false) );
    	bsd = (BinarySearchDesc) s;
     	System.out.println ("* Target = " + bsd.getTarget() + 
        		"\t\t* Find Target In Array: " +  bsd.getFound() + "\n");
    }

    private static String printIntArr (int[] arr)
    {   String outstr = "[";
        int i = 0;
        for( ; i < arr.length-1; i++)
        {  outstr = outstr + arr[i] + "," ;   }
        outstr = outstr + arr[i] + "]";
        return outstr;
    }
}