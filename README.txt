How to test the Programs.

javac QuickSortDriver.java -Xdiags:verbose
java QuickSortDriver

javac  BinarySearchDriver.java -Xdiags:verbose
java  BinarySearchDriver

javac MergSortDriver.java -Xdiags:verbose
java MergSortDriver
