// File BinarySearch.java

public class BinarySearch extends DivConqStrategy
{    
    public boolean isSimple (Problem p)
    {  
//      System.out.println ( "f=" + ((BinarySearchDesc)p).getFirst() + 
 //       		    "  l= " + ((BinarySearchDesc)p).getLast() );

   	 return ( ((BinarySearchDesc)p).getFirst() >=
                 ((BinarySearchDesc)p).getLast()     );
    }

    public Solution simplySolve (Problem p)
    {  	 	
      int first = ((BinarySearchDesc)p).getFirst();
      int target = ((BinarySearchDesc)p).getTarget();
      int[] a   = ((BinarySearchDesc)p).getArr ();
      if ( a[first] == target ){
       		((BinarySearchDesc)p).setFound();
       }
       
       System.out.print( "* Target = " + ((BinarySearchDesc)p).getTarget() );
       System.out.println ( "\t\t* Found = " + ((BinarySearchDesc)p).getFound() );
       
       return (Solution) p ;       
    }
    
    public Problem[] decompose (Problem p)
    {   int first = ((BinarySearchDesc)p).getFirst();
        int last  = ((BinarySearchDesc)p).getLast();
        int target = ((BinarySearchDesc)p).getTarget();
        int[] a   = ((BinarySearchDesc)p).getArr ();

//	 System.out.println("first "+ first + "   last " + last
//	 		  + "   a.length= " + a.length + "   target= " + target  );
    	 		        
//        System.out.print("a[]=");
//        for (int i=first; i<= last; i++){
//        		System.out.print( a[i]+ " ," );
//        }
//        System.out.println("");
        
        Problem[] ps = new BinarySearchDesc[1];
        
        int middle = ( first + last ) /2;
        if ( a[middle] == target ){
        		ps[0] = new BinarySearchDesc(target, a, middle, middle, true);
 //       		((BinarySearchDesc)ps[0]).setFound();
        }
        else if ( a[middle] < target )
        		ps[0] = new BinarySearchDesc(target, a, middle+1, last, false);
        else 
        		ps[0] = new BinarySearchDesc(target, a, first, middle, false);
 		  
// 		  System.out.println("decompose finished");
        return ps;
    }
    
   public  Solution combine (Problem p, Solution[] ss)
   {   
    	((BinarySearchDesc)p).found = ((BinarySearchDesc)ss[0]).getFound();
    	return (Solution) p;  
    	
   }
   
    

}



