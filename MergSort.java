// File MergSort.java

public class MergSort extends DivConqStrategy {

	public  MergSort(){	}		// constructor
	
    public boolean isSimple (Problem p)
    {   
    	return ( ((MergSortDesc)p).getFirst() >=
                 ((MergSortDesc)p).getLast()     );
    }

    public Solution simplySolve (Problem p)
    {   return (Solution) p ;   }   

	// method decompose is called by others, return a sorted array
	public Problem[] decompose (Problem p)
	{
		int first = ((MergSortDesc)p).getFirst();
      int last  = ((MergSortDesc)p).getLast();
      int[] a   = ((MergSortDesc)p).getArr ();
      int aLength = a.length;
 		int [] copyBuffer = new int[a.length]; // temp array needed during merge

/*	     System.out.println("\nfirst "+ first + "   last " + last
	 		  + "   a.length= " + a.length  );
        System.out.print("a[]=");
        for (int i=first; i<= last; i++)
        		System.out.print( a[i]+ " ," );
        System.out.println("");
*/		
     	Problem[] ps = new MergSortDesc[2];		
		// mid is midpoint of subarray	
		if (first < last){
			int mid = (first + last) / 2;
			int iRightFirst = mid + 1;

			int arrLeft []  = new int[mid+1];
			for (int i=0; i< arrLeft.length; i++)
			{	
				arrLeft[i] = a[first++]; 
//				System.out.print ("@" + arrLeft[i] );  
			}
			
//   		System.out.println("\naLength" + aLength);		

			int arrRight [] = new int[aLength-mid-1];
			for (int i=0; i<arrRight.length; i++)
			{	
				arrRight[i] = a[iRightFirst++]; 
//				System.out.print ("@" + arrRight[i] ); 
			}
	
//System.out.println ("\n* decompose1  arrLeft.length=  " + arrLeft.length);
			ps[0] = new MergSortDesc(arrLeft, 0, arrLeft.length-1);
			
//System.out.println ("* decompose2 arrRight.length=  " + arrRight.length+"\n");
			ps[1] = new MergSortDesc(arrRight, 0, arrRight.length-1);

		}
		return ps;
	}
	
	// merge method conbines two sorted subarrays into a larger sorted subarray.
	public  Solution combine (Problem p, Solution[] ss)
	{		
		int left0 = ((MergSortDesc)ss[0]).getFirst();
		int right0 = ((MergSortDesc)ss[0]).getLast();
      int[] ass0 = ((MergSortDesc)ss[0]).getArr ();
//      System.out.println("left0= " + left0 +"   right0= " + right0);
      
		int left1 = ((MergSortDesc)ss[1]).getFirst();
		int right1 = ((MergSortDesc)ss[1]).getLast();
		int[] ass1 = ((MergSortDesc)ss[1]).getArr ();
//		System.out.println("left1= " + left1 +"   right1= " + right1 +"\n");
      
		
		int count = ass0.length + ass1.length;
		int [] copyBuffer = new int [count];

      // combine tow subarray to one array
		for ( int i = 0; i <= count-1; i++ ){  
			if ( left0 > right0 )
				copyBuffer[i] = ass1[left1++];		// First subarray exhausted
			else if (left1 > right1)
				copyBuffer[i] = ass0[left0++];		// Second subarray exhausted
			else 	if (ass0[left0]< ass1[left1] )
				copyBuffer[i] = ass0[left0++];		// Item in 1st subarray is less
			else 
				copyBuffer[i] = ass1[left1++];		// Item in 2nd subarray is less
		}

	((MergSortDesc)p).setCopyBuffer(copyBuffer);
	return (Solution) ( new MergSortDesc (copyBuffer, 0, count-1) );
		
	}
	
	
}
