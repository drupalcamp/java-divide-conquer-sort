// File DivConqContext.java

public class DivConqContext
{    
    private DivConqStrategy dc;    
    
    public DivConqContext (DivConqStrategy dc)
    {   this.dc = dc;   }

    final public Solution solve (Problem p)
    {   Problem[] pp;
        if (dc.isSimple(p))
        {   
//        System.out.println ( "f=" + ((MergSortDesc)p).getFirst() + 
//       	"  l= " + ((MergSortDesc)p).getLast() +"  issimple() " );
        	 return dc.simplySolve(p); 
        }
        else
        {   
        	  pp = dc.decompose(p); 
//        	System.out.println ("* solve ");	
        	}

//        System.out.println ("found = ");

        Solution[] ss = new Solution[pp.length];
        for (int i = 0; i < pp.length; i++)
        {   ss[i] = solve(pp[i]);   } 
        
//        System.out.println ("* sovle finish ");
        
        return dc.combine(p, ss);
    }

    public void setAlgorithm (DivConqStrategy dc)
    {   this.dc = dc;   }


}

