// File QuickSortDriver.java

public class QuickSortDriver
{
    public static void main (String[] args)
    {  
    	  QuickSort qs = new QuickSort();
    	  DivConqContext dcc = new DivConqContext( qs);
    	  
		  System.out.println("* QuickSortDriver to QUICK SORT * Gang Wang  *\n");
    	  Student[] tosort ={ new Student("Gang","162100"), new Student("Tom","162400"), 
    	  			new Student("Mike","163330"), new Student("Liu","162240"), 
    	  			new Student("Rose","162150"),new Student("Tom","262100"),
    	  			new Student("Jerry","169990"), new Student("Mike","163330") };
        
		  System.out.print ("Unsorted Array:\t"  + printStudentArr( tosort) +"\n" );
        
        dcc.solve( new QuickSortDesc(tosort, 0, tosort.length-1) );
        System.out.println("\nSorted by Gang:\t" + printStudentArr(tosort));
        
        System.out.print("\n(" + tosort[6].toStdString() + ") equals to ("
        	 + tosort[7].toStdString() + "): \t" + tosort[6].equalTo(tosort[7])  );

        System.out.print("\n(" + tosort[7].toStdString() + ") greater than ("
        	 + tosort[6].toStdString() + "):\t" + tosort[7].greaterThan(tosort[6])  );
        
    }

    private static String printStudentArr (Student[] arr)
    {   String outstr = "[ ";
        int i = 0;
        for( ; i < arr.length-1; i++)
        {  outstr = outstr + arr[i].toStdString() + " , " ;   }
        outstr = outstr + arr[i].toStdString()+ " ]";
        return outstr;
    }
}