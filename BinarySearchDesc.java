// File BinarySearchDesc.java

public class BinarySearchDesc implements Problem, Solution
{
    private int[]  arr;
    private int    first;
    private int    last;
    private int    target;    
    public boolean found ;
    
    public BinarySearchDesc (int target, int[] arr, int first, int last, boolean found) 
    {   this.arr    = arr;  
        this.first  = first;
        this.last   = last;
        this.target = target;
        this.found  = found;
    }
    public int[] getArr ()   { return arr;   }
    public int   getFirst () { return first; }
    public int   getLast ()  { return last;  }
    public int   getTarget ()  { return target;  }
    
	 public void  setFound() { found = true; }
	 public boolean getFound() { return found; }

}
